package com.dreamcloud.esa_score.score;

import com.dreamcloud.esa_score.analysis.Collection;

import java.io.File;
import java.io.IOException;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicLong;

public class LoggingScoreReader implements DocumentScoreReader {
    private final DocumentScoreReader reader;
    private final File outputFile;
    AtomicLong timeTaken = new AtomicLong(0);
    AtomicLong termsRead = new AtomicLong(0);

    public LoggingScoreReader(DocumentScoreReader reader, File outputFile) {
        this.reader = reader;
        this.outputFile = outputFile;
    }

    public void getTfIdfScores(String term, Vector<TfIdfScore> outVector) throws IOException {
        long startTime = System.nanoTime();
        reader.getTfIdfScores(term, outVector);
        timeTaken.addAndGet(System.nanoTime() - startTime);
        termsRead.incrementAndGet();
    }

    public void getTfIdfScores(String[] terms, Vector<TfIdfScore> outVector) throws IOException {
        long startTime = System.nanoTime();
        reader.getTfIdfScores(terms, outVector);
        timeTaken.addAndGet(System.nanoTime() - startTime);
        termsRead.addAndGet(terms.length);
    }

    private double getTermsReadPerSecond() {
        return termsRead.get() / (timeTaken.get() / 1000000000.0d);
    }

    private long getTermsRead() {
        return termsRead.get();
    }

    public void write(Collection collection) {

    }
}
