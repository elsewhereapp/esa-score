package com.dreamcloud.esa_score.fs;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * The term index represents all of the information about the terms within a collection
 * and is used to lookup document scores in the document score reader.
 *
 * Because it has term info, it is also the place to handle document frequencies for IDF.
 * Sometimes you don't need to lookup existing scores and only need IDF information
 * along with document count and average document length for computing BM25 and TF-IDF
 * scores. This is the class to use for that, though to be honest, doc count and avg doc length
 * really shouldn't be in the "term index" and maybe can be moved directly to the collection at some oint
 * in the future.
 */
public class TermIndex {
    private final double averageDocumentLength;
    private final int documentCount;
    private final Map<String, TermIndexEntry> termIndex = new HashMap<>();
    private Map<String, Integer> documentFrequencies = null;

    public TermIndex(int documentCount, double averageDocumentLength) {
        this.documentCount = documentCount;
        this.averageDocumentLength = averageDocumentLength;
    }

    public TermIndex(int documentCount, double averageDocumentLength, Map<String, Integer> documentFrequencies) {
        this(documentCount, averageDocumentLength);
        this.documentFrequencies = documentFrequencies;
    }

    public void addEntry(TermIndexEntry entry) {
        termIndex.put(entry.term, entry);
    }

    public TermIndexEntry getEntry(String term) {
        return termIndex.get(term);
    }

    public Set<String> getTerms() {
        return termIndex.keySet();
    }

    public int getDocumentCount() {
        return documentCount;
    }

    public Integer getDocumentFrequency(String term) {
        return this.getDocumentFrequencies().get(term);
    }

    public synchronized void buildDocumentFrequencies() {
        if (documentFrequencies == null) {
            Map<String, Integer> frequencies = new HashMap<>();
            for (TermIndexEntry entry : termIndex.values()) {
                frequencies.put(entry.term, entry.documentFrequency);
            }
            documentFrequencies = frequencies;
        }
    }

    public Map<String, Integer> getDocumentFrequencies() {
        if (documentFrequencies == null) {
            buildDocumentFrequencies();
        }
        return documentFrequencies;
    }

    public double getAverageDocumentLength() {
        return this.averageDocumentLength;
    }
}
