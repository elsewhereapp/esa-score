package com.dreamcloud.esa_score.fs;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class DocumentScoreFileReader implements DocumentScoreDataReader {
    //RandomAccessFile scoreFileReader;
    FileInputStream scoreFileReader;

    public DocumentScoreFileReader(File scoreFile) throws FileNotFoundException {
        //scoreFileReader = new RandomAccessFile(scoreFile, "r");
        scoreFileReader = new FileInputStream(scoreFile);
    }

    public ByteBuffer readScores(long offset, int numScores) throws IOException {
        synchronized (scoreFileReader) {
            ByteBuffer buffer = ByteBuffer.allocate(numScores * FileSystem.DOCUMENT_SCORE_BYTES);
            scoreFileReader.getChannel().read(buffer, offset);
            return buffer.rewind();
        }
    }
}
