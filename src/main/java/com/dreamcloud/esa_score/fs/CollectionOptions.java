package com.dreamcloud.esa_score.fs;

import com.dreamcloud.esa_score.score.DocumentNameResolver;
import com.dreamcloud.esa_score.score.DocumentScoreReader;
import com.dreamcloud.esa_score.score.ScoreReader;

import java.io.File;
import java.io.IOException;

public class CollectionOptions {
    private TermIndex termIndex;
    private DocumentScoreReader scoreReader;
    private DocumentNameResolver documentNameResolver;

    public CollectionOptions(File termIndexSource, File documentScoreSource, File idTitlesSource) throws IOException {
        this.setTermIndex(new TermIndexReader(termIndexSource).readIndex());
        this.setScoreReader(new ScoreReader(this.getTermIndex(), new DocumentScoreFileReader(documentScoreSource)));

        if (idTitlesSource != null) {
            this.setDocumentNameResolver(new DocumentNameResolver(idTitlesSource));
        }
    }

    public CollectionOptions(File termIndexSource, File documentScoreSource) throws IOException {
        this(termIndexSource, documentScoreSource, null);
    }

    public CollectionOptions(TermIndex termIndex, DocumentScoreReader scoreReader, DocumentNameResolver nameResolver) {
        this.setTermIndex(termIndex);
        this.setScoreReader(scoreReader);
        this.setDocumentNameResolver(nameResolver);
    }

    public CollectionOptions(TermIndex termIndex, DocumentScoreReader scoreReader) {
        this(termIndex, scoreReader, null);
    }

    public DocumentScoreReader getScoreReader() {
        return scoreReader;
    }

    public void setScoreReader(DocumentScoreReader scoreReader) {
        this.scoreReader = scoreReader;
    }

    public DocumentNameResolver getDocumentNameResolver() {
        return documentNameResolver;
    }

    public void setDocumentNameResolver(DocumentNameResolver documentNameResolver) {
        this.documentNameResolver = documentNameResolver;
    }

    public TermIndex getTermIndex() {
        return termIndex;
    }

    public void setTermIndex(TermIndex termIndex) {
        this.termIndex = termIndex;
    }
}
