package com.dreamcloud.esa_score.fs;

import com.dreamcloud.esa_score.score.TfIdfScore;

import java.io.IOException;

public interface CollectionWriter {
    void writeCollectionInfo(TermIndex termIndex);
    void writeDocumentScores(int documentId, TfIdfScore[] scores);
    void close() throws IOException;
}
