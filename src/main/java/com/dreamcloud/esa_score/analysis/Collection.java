package com.dreamcloud.esa_score.analysis;

import com.dreamcloud.esa_score.fs.CollectionOptions;
import com.dreamcloud.esa_score.fs.TermIndex;
import com.dreamcloud.esa_score.score.DocumentNameResolver;
import com.dreamcloud.esa_score.score.DocumentScoreReader;
import com.dreamcloud.esa_score.score.LoggingScoreReader;
import com.dreamcloud.esa_score.score.TfIdfScore;

import java.io.IOException;
import java.util.Map;
import java.util.Vector;

public class Collection {
    private TermIndex termIndex;
    private DocumentScoreReader scoreReader;
    private DocumentNameResolver nameResolver;
    private Map<String, Integer> documentFrequencyCache = null;
    private final LoggingScoreReader log;

    public Collection(TermIndex termIndex, DocumentScoreReader scoreReader, DocumentNameResolver nameResolver) {
        this.termIndex = termIndex;
        this.scoreReader = scoreReader;

        if (scoreReader instanceof LoggingScoreReader) {
            //special stuff that we really shouldn't do!
            log = (LoggingScoreReader) scoreReader;
        } else {
            log = null;
        }

        this.nameResolver = nameResolver;
    }

    public Collection(TermIndex termIndex, DocumentScoreReader scoreReader) {
        this(termIndex, scoreReader, null);
    }

    public Collection(CollectionOptions options) {
        this(options.getTermIndex(), options.getScoreReader(), options.getDocumentNameResolver());
    }

    public int getDocumentFrequency(String term) {
        return this.getDocumentFrequencies().getOrDefault(term, 0);
    }

    public boolean hasDocumentFrequency(String term) {
        return this.getDocumentFrequencies().containsKey(term);
    }

    public Map<String, Integer> getDocumentFrequencies() {
        return termIndex.getDocumentFrequencies();
    }

    public int getDocumentCount() {
        return this.termIndex.getDocumentCount();
    }

    public double getAverageDocumentLength() {
        return this.termIndex.getAverageDocumentLength();
    }

    public int getDocumentId(String documentTitle) {
        if (nameResolver == null) {
            return -1;
        }
        return nameResolver.getId(documentTitle);
    }

    public String getDocumentTitle(int documentId) {
        if (nameResolver == null) {
            return null;
        }
        return nameResolver.getTitle(documentId);
    }

    public void getTfIdfScores(String term, Vector<TfIdfScore> outVector) throws IOException {
        scoreReader.getTfIdfScores(term, outVector);
    }

    public void getTfIdfScores(String[] terms, Vector<TfIdfScore> outVector) throws IOException {
        scoreReader.getTfIdfScores(terms, outVector);
    }

    public void writeLogs() {
        if (log != null) {
            log.write(this);
        }
    }
}
